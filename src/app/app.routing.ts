import { Routes, RouterModule } from '@angular/router';



export const AppRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        loadChildren: './modules/dasboard/default.module#DefaultModule'
      },
     
    ]
 
  },
] 
