import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './../../shared/material/material.module';
import { SidebarComponent } from './../../shared/components/sidebar/sidebar.component';
import { FooterComponent } from './../../shared/components/footer/footer.component';
import { DefaultComponent } from './components/default/default.component';
import { UsageComponent } from './components/usage/usage.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { StockComponent } from './components/stock/stock.component';
import { RequisitionComponent } from './components/requisition/requisition.component';
import { MenuComponent } from './components/menu/menu.component';
import { InfoComponent } from './components/info/info.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardRoutes } from './dashboard.routing';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from 'src/app/shared/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,

    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [
    DashboardComponent,
    InfoComponent,
    MenuComponent,
    RequisitionComponent,
    StockComponent,
    ToolbarComponent,
    UsageComponent,
    DefaultComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent
  ]
})
export class DefaultModule { }
