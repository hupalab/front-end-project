import { Component, OnInit, OnDestroy } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit, OnDestroy {
  sideBarOpen = true;

  watcher: Subscription;
  activeMediaQuery = '';
  /*
    Inital watcher accept MediaChange when contructor
    Destroy watcher

  */
  constructor(public mediaObserver: MediaObserver) {
    this.watcher = mediaObserver.media$.subscribe((change: MediaChange) => {
      // console.log(change);
      this.activeMediaQuery = change ? `'${change.mqAlias}' = (${change.mediaQuery})` : '';
      if ( change.mqAlias == 'xs') {
        console.log("xs");
      }
      if ( change.mqAlias == 'sm') {
        console.log("sm");
      }
      if ( change.mqAlias == 'md') {
        console.log("md");
      }
      if ( change.mqAlias == 'lg') {
        console.log("lg");
      }
      if ( change.mqAlias == 'xl') {
          this.loadMobileContent();
      }
    });
   }

  ngOnInit() {
    console.log(this.activeMediaQuery);
  }

  loadMobileContent() {
    console.log("action ... at sceen xl");
    throw new Error("Method not implemented.");
  }
  ngOnDestroy() {
    this.watcher.unsubscribe();
  }

  sideBarToggler($event) {
    this.sideBarOpen = !this.sideBarOpen;
  }
}
