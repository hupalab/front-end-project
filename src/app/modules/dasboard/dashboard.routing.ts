import { UsageComponent } from './components/usage/usage.component';
import { DefaultComponent } from './components/default/default.component';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';

export const DashboardRoutes: Routes = [
  {
    path:'',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'usage',
        component: UsageComponent
      }
    ]
  }
]
